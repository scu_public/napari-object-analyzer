---
title: Writing napari plug-ins
author: Aaron Ponti
papersize: a4
geometry: margin=1in
header-includes:
  - \hypersetup{colorlinks=True, linkcolor=black, urlcolor=blue}
---

# Course material

The code of this tutorial can be found on [https://git.bsse.ethz.ch/scu_public/napari-object-analyzer](https://git.bsse.ethz.ch/scu_public/napari-object-analyzer).

# Installing napari

## Installing conda

Download and install **miniconda** for your operating system from [https://docs.conda.io/en/latest/miniconda.html](https://docs.conda.io/en/latest/miniconda.html). You can find detailed installation instructions in our wiki: [https://wiki-bsse.ethz.ch/display/SCF/Install+miniconda+on+Windows,+macOS,+and+Linux](https://wiki-bsse.ethz.ch/display/SCF/Install+miniconda+on+Windows,+macOS,+and+Linux).

## Installing napari in a dedicated environment

On Windows, start **Anaconda prompt**; on macOS or Linux start your **terminal**. Create a virtual environment for Python and install napari with the following instructions (`$` is the terminal prompt):

```bash
$ conda create -y -n napari-env python=3.8
$ conda activate napari-env
$ pip install "napari[all]"
```

## Testing the installation

From the Anaconda prompt or your terminal, activate the `napari-env` virtual environment and start napari:

```bash
$ conda activate napari-env
$ napari
```

![The napari user interface.](images/napari.png){height=300px}

Alternatively, you can start napari from the Python (or IPython) terminal:

```bash
$ ipython
```

```python
Python 3.8.11 (default, Aug  3 2021, 15:09:35) 
Type 'copyright', 'credits' or 'license' for more information
IPython 7.26.0 -- An enhanced Interactive Python. Type '?' for help.

In [1]: from skimage import data
In [2]: import napari
In [3]: viewer = napari.view_image(data.astronaut(), rgb=True)
```

![napari with the astronaut demo image.](images/napari_with_astronaut.png){height=300px}

In a script, you would have to add a call to `napari.run()` to start the event loop of the user interface library.

```python
# my_script.py
from skimage import data
import napari
viewer = napari.view_image(data.astronaut(), rgb=True)
napari.run()
```

And then from console:

```bash
$ python my_script.py
```

![napari with the astronaut demo image (from a script).](images/napari_with_astronaut.png){height=300px}

napari can also be started (and interacted with) from a [Jupyter notebook](https://jupyter.org/): see [https://napari.org/tutorials/fundamentals/getting_started.html#jupyter-notebook-usage](https://napari.org/tutorials/fundamentals/getting_started.html#jupyter-notebook-usage) for more details.

# Fundamentals of napari

The [napari.org](https://napari.org) website offers a list of tutorials on [https://napari.org/tutorials/index.html](https://napari.org/tutorials/index.html).  napari defines several main objects that we interact with. The **Viewer** ([https://napari.org/tutorials/fundamentals/viewer.html](https://napari.org/tutorials/fundamentals/viewer.htm)) is the main object in napari. It organizes the data we load and generate and provides us with a series of graphical and programmatical tools to interact with our raw and derived data. From the user perspective, the Viewer is the user interface that is presented to us when we start napari. 

![The Viewer and its main UI components.](images/viewer_layout.jpg)

The Viewer is divided into a few areas:

* the **canvas** displays the data and all objects generated;
* the **layer list** manages all layers that are shown in the canvas: a layer can be used to display images, points, shapes, and other data types; for extended tutorials on the various types of layers please see:
  * for *image layers*: [https://napari.org/tutorials/fundamentals/image.html](https://napari.org/tutorials/fundamentals/image.html); 
  * for *label layers*: [https://napari.org/tutorials/fundamentals/labels.html](https://napari.org/tutorials/fundamentals/labels.html);
  * for *point lyers*: [https://napari.org/tutorials/fundamentals/points.html](https://napari.org/tutorials/fundamentals/points.html);
  * for *shapes layers*:[https://napari.org/tutorials/fundamentals/shapes.html]( https://napari.org/tutorials/fundamentals/shapes.html):
  * for *surface lyers*: [https://napari.org/tutorials/fundamentals/surface.html](https://napari.org/tutorials/fundamentals/surface.html);
  * for *tracks layers*: [https://napari.org/tutorials/fundamentals/tracks.html](https://napari.org/tutorials/fundamentals/tracks.html);
  * for *vectors layers*: [https://napari.org/tutorials/fundamentals/vectors.html](https://napari.org/tutorials/fundamentals/vectors.html).
* the **layer controls** allow changing parameters for the corresponding layers selected in the  layer list below;
* the **layer buttons** allow creation of new layers and deletion of existing ones;
* the **dimension sliders** allow navigation of N-dimensional datasets;
* the **viewer buttons** allow toggling the IPython console, switching between 2D and 3D rendering, switching between dimensions to be displayed (e.g. from XY to XZ) or transposing them, and a *home* button to reset the camera;
* the **status bar** displays useful information.

The API to programmatically interact with all napari objects can be found on [https://napari.org/api/stable/](https://napari.org/api/stable/). As an example, this is the full documentation of the `napari.Viewer` class: [https://napari.org/api/stable/napari.Viewer.html#napari.Viewer](https://napari.org/api/stable/napari.Viewer.html#napari.Viewer), with the corresponding source code: [https://napari.org/_modules/napari/viewer.html#Viewer](https://napari.org/_modules/napari/viewer.html#Viewer).

# Writing custom napari plugins

A software providing a plug-in mechanism offers a clean way for third-parties to integrate new functionalities in the software without forcing them to access and modify the original code. In the case of proprietary software, for instance, the code is not even accessible or documented. And even if it were, keeping the new functionalities as *independent* as possible from the core software makes both codes more robust against breakage due to future API changes. A clean way to provide support for third-party extensions to a given software is to define **interfaces** that allow the tools to interact and exchange data over well-defined **function signatures**.

## Enabling/disabling existing plug-ins

napari comes with a couple pre-installed and a growing list of installable plug-ins. You can see the complete list on the napari-hub at [https://www.napari-hub.org](https://www.napari-hub.org). Existing plug-ins can be listed, enabled and disabled directly from napari via the `Plugins > Install/Uninstall Package(s)` menu. In this section, we will write our own simple plug-in. For the documentation, please see [https://napari.org/plugins/stable/index.html](https://napari.org/plugins/stable/index.html).

## Fundamental concepts of napari plug-ins

Two fundamental concepts in the development of napari plug-ins are the `napari_hook_specification` and the corresponding `napari_hook_implementation`. A hook specification provides one or more **call signatures** (i.e. functions definitions) that a hook implementation may decide to implement. Many implementations can be registered for each specification, so for a specification that defines a file reader, for example, any number of file reader implementations can be registered. In practice, the implementation must be decorated with the `@napari_hook_implementation` decorator and define the expected function signature, for example:

```python
@napari_hook_implementation
def napari_get_reader(path):
    """Reads a new file type that napari can't read yet."""
    ...
```

Currently, napari supports **Input/Output** (readers and writers), **Analysis**, and **GUI** plug-ins. They all come with their **hook specifications**. For detailed description of all hook specifications supported by napari, please see [https://napari.org/plugins/stable/hook_specifications.html](https://napari.org/plugins/stable/hook_specifications.html). For convenience, the next subsections list the current hooks specifications with direct links to their documentation.

### IO hooks

* `napari_provide_sample_data`: Provide sample data ([docs](https://napari.org/plugins/stable/hook_specifications.html#napari.plugins.hook_specifications.napari_provide_sample_data)).
* `napari_get_reader`: Return a function capable of loading `path` into napari, or `None` ([docs](https://napari.org/plugins/stable/hook_specifications.html#napari.plugins.hook_specifications.napari_get_reader)).
* `napari_get_writer`: Return function capable of writing napari layer data to `path` ([docs](https://napari.org/plugins/stable/hook_specifications.html#napari.plugins.hook_specifications.napari_get_writer)).

#### Single-layer IO hooks

* `napari_write_image`: Write image data and metadata into a path ([docs](https://napari.org/plugins/stable/hook_specifications.html#napari.plugins.hook_specifications.napari_write_image)).
* `napari_write_labels`: Write labels data and metadata into a path ([docs](https://napari.org/plugins/stable/hook_specifications.html#napari.plugins.hook_specifications.napari_write_labels)).
* `napari_write_points`: Write points data and metadata into a path ([docs](https://napari.org/plugins/stable/hook_specifications.html#napari.plugins.hook_specifications.napari_write_points)).
* `napari_write_shapes`: Write shapes data and metadata into a path ([docs](https://napari.org/plugins/stable/hook_specifications.html#napari.plugins.hook_specifications.napari_write_shapes)).
* `napari_write_surface`: Write surface data and metadata into a path ([docs](https://napari.org/plugins/stable/hook_specifications.html#napari.plugins.hook_specifications.napari_write_surface)).
* `napari_write_vectors`: Write vectors data and metadata into a path ([docs](https://napari.org/plugins/stable/hook_specifications.html#napari.plugins.hook_specifications.napari_write_vectors)).

### Analysis hooks

* `napari_experimental_provide_function`: Provide function(s) that can be passed to [magicgui](https://napari.org/guides/stable/magicgui.html?highlight=magicgui) ([docs](https://napari.org/plugins/stable/hook_specifications.html#napari.plugins.hook_specifications.napari_experimental_provide_function)).

### GUI hooks

* `napari_experimental_provide_theme`: Provide GUI with a set of colors used through napari ([docs](https://napari.org/plugins/stable/hook_specifications.html#napari.plugins.hook_specifications.napari_experimental_provide_theme)).
* `napari_experimental_provide_dock_widget`: Provide functions that return widgets to be docked in the viewer ([docs](https://napari.org/plugins/stable/hook_specifications.html#napari.plugins.hook_specifications.napari_experimental_provide_dock_widget)).

## Creating a new plug-in

napari makes it easy to initialize a plug-in development environment with all components in place for developing, documenting, testig and deploying our new plug-in[^init_plugins]. 

[^init_plugins]: Why not strictly necessary, it is recommended to follow these instructions to set up a plug-in that can then easily be installed and even published to the [napari hub](https://www.napari-hub.org).

### Initialize the plug-in structure

To initialize the structure of our plug-in, we will use the **cookiecutter** tool (see [https://cookiecutter.readthedocs.io/en/latest/](https://cookiecutter.readthedocs.io/en/latest/) for the latest documentation). We can use pip to install it:

```bash
$ pip install cookiecutter
```

Since the configuration for napari plug-ins is already defined, we only need to run the following call to set up our plug-in backbone[^mkdocs]!

[^mkdocs]: For documenting our code, we will choose [MkDocs](https://www.mkdocs.org/), but we won't discuss how to actually write and create the documentation.

```bash
$ cookiecutter https://github.com/napari/cookiecutter-napari-plugin
full_name [Napari Developer]: Aaron Ponti
email [yourname@example.com]: aaron.ponti@bsse.ethz.ch
github_username_or_organization [githubuser]: aarpon
plugin_name [napari-foobar]: napari-object-analyzer
Select github_repository_url:
1 - https://github.com/aarpon/napari-object-analyzer
2 - provide later
Choose from 1, 2 [1]: 1
module_name [napari_object_analyzer]: noa
short_description [A simple plugin to use with napari]: napari plug-in
   for simple 2D object detection and analysis.         
include_reader_plugin [y]: y
include_writer_plugin [y]: y
include_dock_widget_plugin [y]: y
include_function_plugin [y]: y
use_git_tags_for_versioning [n]: n
Select docs_tool:
1 - mkdocs
2 - sphinx
3 - none
Choose from 1, 2, 3 [1]: 1
Select license:
1 - BSD-3
2 - MIT
3 - Mozilla Public License 2.0
4 - Apache Software License 2.0
5 - GNU LGPL v3.0
6 - GNU GPL v3.0
Choose from 1, 2, 3, 4, 5, 6 [1]: 4

Your plugin template is ready!  Next steps:
...
```

Let's now quickly discuss the *next steps*: we will not go as far as discussing how to upload our plug-in to the napari hub, since we will only play around for today. For more information about this part, please see [Make your plugin discoverable](https://napari.org/plugins/stable/for_plugin_developers.html#step-3-make-your-plugin-discoverable), [Preparing for release](https://napari.org/plugins/stable/for_plugin_developers.html#step-4-preparing-for-release) and [Share your plugin with the world](https://napari.org/plugins/stable/for_plugin_developers.html#step-5-share-your-plugin-with-the-world). 

#### Next steps

Let's intialize a git repository and commit the first version of our plug-in.

```bash
$ cd napari-object-analyzer
$ git init -b main
$ git add .
$ git config --global user.name "Aaron Ponti"
$ git config --global user.email "aaron.ponti@bsse.ethz.ch"
$ git commit -m "Initial commit."
[main (root-commit) 56b12f4] Initial commit.
 22 files changed, 893 insertions(+)
 create mode 100644 .github/workflows/test_and_deploy.yml
 create mode 100644 .gitignore
 create mode 100644 .napari/DESCRIPTION.md
 create mode 100644 LICENSE
 create mode 100644 MANIFEST.in
 create mode 100644 README.md
 create mode 100644 docs/index.md
 create mode 100644 mkdocs.yml
 create mode 100644 noa/__init__.py
 create mode 100644 noa/_dock_widget.py
 create mode 100644 noa/_function.py
 create mode 100644 noa/_reader.py
 create mode 100644 noa/_tests/__init__.py
 create mode 100644 noa/_tests/test_dock_widget.py
 create mode 100644 noa/_tests/test_function.py
 create mode 100644 noa/_tests/test_reader.py
 create mode 100644 noa/_tests/test_writer.py
 create mode 100644 noa/_writer.py
 create mode 100644 requirements.txt
 create mode 100644 setup.cfg
 create mode 100644 setup.py
 create mode 100644 tox.ini
```

And we can immediately install it in our `napari-env` environment (in editable form, so that we can keep editing it and debugging it without having to reinstall it every time):

```bash
$ pip install -e .
```

If you don't have a [github.com](https://github.com) repository for your plug-in yet, you can create one with the same name as the local plug-in (in our case `napari-object-analyzer`) on [https://github.com/new](https://github.com/new). Once your repository is up an running, you can add it as a **remote** to our local one and push:

```bash
$ git remote add origin https://github.com/aarpon/napari-object-analyzer.git
$ git push -u origin main
```

Please notice that if you do not plan to share your plug-in, you do not need to create a [github.com](https://github.com) repository or push to it.

If we want to make our plug-in discoverable from napari's `Plugins > Install/Uninstall Plugins...` by publishing it to the [napari hub](https://www.napari-hub.org/), please see the documentation at [Make your plugin discoverable](https://napari.org/plugins/stable/for_plugin_developers.html#step-3-make-your-plugin-discoverable), [Preparing for release](https://napari.org/plugins/stable/for_plugin_developers.html#step-4-preparing-for-release) and [Share your plugin with the world](https://napari.org/plugins/stable/for_plugin_developers.html#step-5-share-your-plugin-with-the-world).  

### Directory structure of our plug-in

Let's have a look at the directory structure of our newly created plug-in. Our plug-in will be written in Python and will be contained in the module `noa`.

```bash
$ tree
.
├── .github
│   └── workflows
│   │   └── test_and_deploy.yml
├── .napari
│   └── DESCRIPTION.md
├── docs
│   └── index.md
├── LICENSE
├── MANIFEST.in
├── mkdocs.yml
├── noa
│   ├── _dock_widget.py
│   ├── _function.py
│   ├── __init__.py
│   ├── _reader.py
│   ├── _tests
│   │   ├── __init__.py
│   │   ├── test_dock_widget.py
│   │   ├── test_function.py
│   │   ├── test_reader.py
│   │   └── test_writer.py
│   └── _writer.py
├── README.md
├── requirements.txt
├── setup.cfg
├── setup.py
└── tox.ini
```

Let's quickly go through the structure of the project.

* `.github/workflows/test_and_deploy.yml`: workflow configuration to upload the published package to the [Python Package Index](https://pypi.org/) (see [https://docs.github.com/en/actions/guides/building-and-testing-python#publishing-to-package-registries](https://docs.github.com/en/actions/guides/building-and-testing-python#publishing-to-package-registries))
* `.napari/DESCRIPTION.md`: plug-in description that will be displayed on the [napari hub](https://www.napari-hub.org/).
* `docs`: MkDocs will generate the documentation here.
* `LICENSE`: contains the text of the selected license (in our example, the Apache License Version 2.0, [http://www.apache.org/licenses/](http://www.apache.org/licenses/))
* `MANIFEST.in`: specifies extra files needed for a **source distribution** of your code. A source distrubution still needs to be build before it is installed; in contrast, a **built distribution** is ready to be installed, i.e. all files and folders only need to be moved to the appropriate locations in the target system.
* `mkdocs.yml`: YAML configuration file for the MkDocs project documentation generator.
* `noa`: our Python module that will implement the desired plug-ins. See detailed description below.
* `README.md`: read-me file for your project. It will be rendered on the project page on [github](https://github.com). It explains what the plug-in is all about and provides installation instructions, licensing information, links to issue trackers. You should edit this document appropriately.
* `requirements.txt`: this file contains the dependencies needed for **development**, that is everything that is needed to *run* the plug-in and everything needed for *developing* it (e.g. unit testing, documentation generators, ...). Install them using: `pip install -r requirements.txt`.
* `setup.py` and `setup.cfg`: when the production version of your plug-in is installed on your users' machine, a combination of `setup.py` and `setup.cfg` is needed:
  * `setup.cfg`: configuration file. It contains metadata information about author, software, URLs, licensings, ... and the list of dependences under `install_requires`. Notice that you can also specify the minimum required Python version under `options`. When publishing to the [napari hub](https://www.napari-hub.org/), some necessary fields must be filled (see [Share your plugin with the world](https://napari.org/plugins/stable/for_plugin_developers.html#step-5-share-your-plugin-with-the-world)).
  * `setup.py`: script that actually performs the installation; when installing from napari, this step will be hidden. Manual installation would require: `pip install setup.py`. For an editable installation, use `pip install -e .`.
* `tox.ini`: this file contains the configuration for the [tox](https://tox.readthedocs.io/en/latest/) automated testing tool.

## Developing our plug-in

We want to develop a simple plug-in to find objects in an image and run some simple analysis on them. Specifically, we want to develop it as a **GUI plug-in**. In the process, we will also discuss the various types of plug-ins that can be developed for napari.

Our code will be contained in the `noa` folder. This corresponds to the `noa` Python module that will contain our hook implementations.

```bash
❯ tree noa/
noa/
├── _dock_widget.py
├── _function.py
├── __init__.py
├── _reader.py
├── _tests
│   ├── __init__.py
│   ├── test_dock_widget.py
│   ├── test_function.py
│   ├── test_reader.py
│   └── test_writer.py
└── _writer.py
```

Since we decided to enable all four plug-in types in the cookiecutter, we have:

* `_dock_widget.py`: a **GUI** plug-in;
* `_function.py`: an **Analysis** plug-in;
* `_reader.py`: an **IO** plug-in of type *reader*;
* `_writer.py`: an **IO** plug-in of type *writer*.

Each plug-in has the corresponding unit test in `_tests`[^unit_tests].

[^unit_tests]: We will not discuss writing unit tests in this introductory course.

We will quickly have a look and discuss the example implementations of the `reader`, `writer` and `function` plug-ins from the documentation, and then implement a toy example for a `dock_widget` plugin of our own.

### Fundamental concepts

As we briefly introduced earlier, napari uses **hook specifications** to clearly define types of functionality that plug-ins may implement. A hook specification declares the exact **function signature** that a plug-in must implement to be called sussessfully by napari. Having the correct signature is not enough for a function to be a valid plug-in: the implementation must also be explicitly marked with the `@napari_hook_implementation` **decorator** to be registered as plug-in.

### A `reader` plugin

In this example, we discuss the example implementation from the documentation ([https://napari.org/plugins/stable/for_plugin_developers.html#step-2-write-your-hook-implementation](https://napari.org/plugins/stable/for_plugin_developers.html#step-2-write-your-hook-implementation)) that creates a new reader that can open Numpy binary files. A reader does not only read image data; it can read anything than can be passed on to a **layer**, that is one of `Image`, `Labels`, `Points`, `Shapes`, `Surfaces` `Tracks`, or `Vectors` (see [https://napari.org/api/stable/napari.layers.html](https://napari.org/api/stable/napari.layers.html)).

Please notice that the example implementation in `_reader.py` created by the cookiecutter napari template is similar but not quite the same.

```python
import numpy as np
from napari_plugin_engine import napari_hook_implementation

def npy_file_reader(path):
    array = np.load(path)
    # return it as a list of LayerData tuples,
    # here with no optional metadata
    return [(array,)]

@napari_hook_implementation
def napari_get_reader(path):
    # remember, path can be a list, so we check its type first...
    # (this example plugin doesn't handle lists)
    if isinstance(path, str) and path.endswith(".npy"):
        # If we recognize the format, we return the actual reader function
        return npy_file_reader
    # otherwise we return None.
    return None
```

We start by `import`ing `numpy` to get access to the `numpy.load()` function, and `napari_hook_implementation` from `napari_plugin_engine` to **decorate** the `napari_get_reader()` method[^hook_name].

[^hook_name]: Please notice that you are allowed to call `napari_get_reader()` with any arbitrary name, but then you have to inform napari what hook implementation you mean by setting the `specname` parameter of the `@napari_hook_implementation` decorator as follows: `napari_hook_implementation(specname="napari_get_reader")`.

The `napari_get_reader(path)` method is marked as the **hook implementation** of the corresponding `get_reader(path)` **hook specification**: [https://napari.org/plugins/stable/hook_specifications.html#napari.plugins.hook_specifications.napari_get_reader](https://napari.org/plugins/stable/hook_specifications.html#napari.plugins.hook_specifications.napari_get_reader).

The specification tells us that the hook implementation:

* accepts a `path` or **list** of `path`s,
* must return a function capable of loading `path` into napari, or `None`.

So, the task of the `napari_get_reader(path)` function is to check whether the file specified by the `path` argument can be read by our reader plug-in or not: it does not actually read the file! If it can, then it will return a function that napari can call to perform the actual reading at a later stage.

Let's have a look at `napari_get_reader(path)`: indeed, it checks if the incoming path is a `str`ing that ends with `.npy` (the extension of the Numpy binary file) and, if this is the case, returns the **function** `npy_file_reader(path)` that napari will subsequently call to read the file specified by `path`, or **`None`** to specify that our implementation does not know how to open the file specified by `path`. Please notice that this implementation does not read lists of paths; it `path` is a list, then the function will return `None`, thus informing napari that it has to keep looking for another reader.

The documentation tells us that the function returned by `napari_get_reader()` must be *"a function that accepts the path, and returns a list of `layer_data`, where `layer_data` is one of `(data,)`, `(data, meta)`, or `(data, meta, layer_type)`. If unable to read the path, must return `None` (not `False`!).*

* `data` actual data to be added to the new layer;
* `meta`: a dictionary of arguments that will be passed on the the corresponding `add_{layer}` method;
* `layer_type`: type of the layer to be created (one of `"image"`, `"labels"`,  `"points"`, `"shapes"`,  `"surface"`, `"tracks"`,  `"vectors"`); it defaults to `"image"` (or `"labels"`, depending on the data type).

Our `npy_file_reader()` function indeed returns `[(array, )]` , which is a **list** containing a **tuple** that matches the first option: `(data, )`.  This means that our reader is just returning the pixel data to display, but it won't return any associated `meta` to be passed to `add_image()`[^add_image_meta] , and will fall back to the default layer_type `"image"`. If the loaded data were a `labels` image, for instance, we would set `layer_type = "labels"`[^layer_metadata].

[^add_image_meta]: See [https://napari.org/api/stable/napari.Viewer.html#napari.Viewer.add_image](https://napari.org/api/stable/napari.Viewer.html#napari.Viewer.add_image): `meta` corresponds to all parameters in the function signature that follow the `*`.
[^reader_function_name]: We can call our function however we want, since it is returned as a **callable object**, and python won't need its name to execute it.
[^layer_metadata]: Please see [https://napari.org/api/stable/napari.Viewer.html#napari.Viewer.add_labels](https://napari.org/api/stable/napari.Viewer.html#napari.Viewer.add_labels) for possible values of `meta` to be passed to `add_labels`.

The implementation in our default `_reader.py` implementation is a bit more verbose (and also supports passing a list of paths as argument), but works fundamentally the same.

We can test our (default) reader with a `.npy` file that we can create as follows:

```python
import numpy as np
img = (100 + 25 * np.random.randn(5, 256, 256)).astype(np.uint8) # (z,y,x)
np.save("/home/aaron/Desktop/numpy_array.npy", img)
```

### A `writer` plugin

A `writer` plug-in must implement the `napari_get_writer()` and the `napari_write_image()` **hook specifications**. For simplicity, we will only support saving "Save selected layer(s)...".

```python
import numpy as np
from napari_plugin_engine import napari_hook_implementation

def writer_function(path, layer_data):
    """The writer function would be returned by napari_get_writer()"""
    return None

@napari_hook_implementation
def napari_get_writer(path, layer_types):
    """If 'Save all layers...' is picked, we ignore it."""
    return None

@napari_hook_implementation
def napari_write_image(path, data, meta):
    """This is called when 'Save selected layer(s)...' is picked."""
    
    # If we can save successfully, we return path; 
    # otherwise, we return None
    if str(path).endswith(".npy"):
        try:
            # For simplicity, we ignore the metadata
            np.save(path, data)
        except:
            return None
        return path
    return None
```

Please notice: if there **is** only one layer, the napari will fall back to `napari_write_image()`.

### An `analysis` plugin

An `analysis` plug-in must implement the `napari_experimental_provide_function()` **hook implementation**. The analysis plug-in will appear in the `Plugins > napari-object-analyzer` menu.

Let's just have a look at the example implementation in `_function.py`:

```python
from typing import TYPE_CHECKING
from enum import Enum
import numpy as np
from napari_plugin_engine import napari_hook_implementation
if TYPE_CHECKING:
    import napari

# This is the actual plugin function, where we export our function
# (The functions themselves are defined below)
@napari_hook_implementation
def napari_experimental_provide_function():
    # we can return a single function
    # or a tuple of (function, magicgui_options)
    # or a list of multiple functions with or without options, as shown here:
    return [threshold, image_arithmetic]

# 1.  First example, a simple function that thresholds an image and creates a labels layer
def threshold(data: "napari.types.ImageData", threshold: int) -> "napari.types.LabelsData":
    """Threshold an image and return a mask."""
    return (data > threshold).astype(int)

# 2. Second example, a function that adds, subtracts, multiplies, or divides two layers
# using Enums is a good way to get a dropdown menu.  Used here to 
# select from np functions
class Operation(Enum):
    add = np.add
    subtract = np.subtract
    multiply = np.multiply
    divide = np.divide

def image_arithmetic(
    layerA: "napari.types.ImageData", operation: Operation, layerB: "napari.types.ImageData"
) -> "napari.types.LayerDataTuple":
    """Adds, subtracts, multiplies, or divides two same-shaped image layers."""
    return (operation.value(layerA, layerB), {"colormap": "turbo"})
```

The example defines two functions: `threshold(data, threshold)` and `image_arithmetic(layerA, operation, layerB)`; those are returned in a list from `napari_experimental_provide_function()`: `return [threshold, image_arithmetic]`.

Please notice that these functions are annotated with Python type information, because **magicgui** (see [https://napari.org/magicgui](https://napari.org/magicgui)) needs them to generate appropriate widgets to expose the arguments.

In the next subsections, we will discuss the example analysis plug-ins in more detail.


#### Thresholding plug-in

Let's test the `threshold` plug-in on the sample image `scikit-image > coins`. Selecting `Plugins > napari-object-analyzer > threshold`, a panel is opened on the right. magicgui knows that it needs to generate a widget to pick an image (since the type od `data` is `napari.types.ImageData`) and one to modify the value of the `int`eger argument `threshold` (see the definition of the `threshold()` method above); magicgui will also add a `Run` button that will allow us to run the plug-in[^magicgui_analysis]. 

[^magicgui_analysis]: Please notice that even if the plug-in does not define any type to be passed on to magicgui, the plug-in will get a `Run` button to be triggered.

The `threshold()` function sets all pixel intensities in the image larger to `threshold` to `True` and then converts the result to integer (such that the background is 0, and the foreground is 1) to create a valid **label image**.

```python
return (data > threshold).astype(int)
```

And this is the result of our threshold.

![napari with the result of the threshold `analysis` plug-in.](images/napari_threshold_plugin.png){height=300px}

##### A little digression

We can implement a similar fuctionality interactively in the integrated IPython console (click on the ![](./images/napari_ipython_button.png){height=12px} button to start): 

```python
In [1]: from skimage.filters import threshold_otsu
In [2]: img = viewer.layers[0].data
In [3]: t = threshold_otsu(img) 
In [4]: t
Out[4]: 107
In [5]: labels = img > t            # This returns a Boolean array
In [6]: labels = labels.astype(int) # Convert to int -> labels
In [7]: viewer.add_labels(labels)
Out[7]: <Labels layer 'labels' at 0x7fc2e39eee20>
```

#### Image arithmetic plug-in

To test the image arithmetic plug-in, let's open the IPython console (![](./images/napari_ipython_button.png){height=12px}) and create two images for testing:

```python
In [1]: import numpy as np
In [1]: viewer.add_image(np.ones((256, 256), dtype=np.uint8), name="ones")
Out[2]: <Image layer 'ones' at 0x7fa0e4c91670>
In [3]: viewer.add_image(2 * np.ones((256, 256), dtype=np.uint8), name="twos")
Out[3]: <Image layer 'twos' at 0x7fa0e4c966d0>
```

Now, we can run the plug-in from `Plugins > napari-object-analyzer > image arithmetic`. We set the following parameters:

![Image arithmetic widget](images/napari_image_arithmetic_widget.png){height=67px}

If we run it, the `ones` and `two` images will be summed, and a new image layer will be added.

![napari with the result of the image arithmetic `analysis` plug-in.](images/napari_image_arithmetic_result.png){height=300px}

If you hover your mouse pointer over the image, you will see that all pixel intensities are $3$ as expected.

### A `GUI` plug-in

A GUI plug-in provides one or more functions that return widgets that can be docked in the viewer. A GUI plug-in supports magicgui as in the case of the analysis plug-in above, or PyQt5/PySide2 widgets. The latter widgets are way more complex than magicgui ones and are beyond the scope of our quick introduction (for a rather extensive example, please see [https://github.com/aarpon/qu](https://github.com/aarpon/qu)). Thus, we will develop a simpler magicgui-based one in this example.

```python
# Default examples not shown

def object_detector_and_analyzer_wrapper():
    """Wrapper to the plug-in: napari will call this function
    to get the dock widget."""

    @magicgui(
        img_layer=dict(label='Image'),
        min_sigma=dict(label="Min sigma"),
        max_sigma=dict(label="Max sigma"),
        num_sigma=dict(label="Num sigma"),
        threshold=dict(label="Threshold", step=0.01),
    )
    def plugin(
            img_layer: napari.layers.Image,
            min_sigma: int = 3,
            max_sigma: int = 15,
            num_sigma: int = 13,
            threshold: float = 0.05
    ):
        """Simple detection and analysis function."""

        # If there is no image loaded, return here
        if img_layer is None:
            return

        # Get the viewer
        viewer = napari.current_viewer()

        # Make sure we work on a gray-value image
        image = np.asarray(img_layer.data)
        if image.ndim > 2 and image.shape[2] == 3:
            image = rgb2gray(image)

        # Run blob detector based on Laplacian of Gaussian
        blobs = blob_log(image,
                         min_sigma=min_sigma,
                         max_sigma=max_sigma,
                         num_sigma=num_sigma,
                         threshold=threshold)

        # We compute the radii from the 3rd columns (that contains
        # the winning sigma)
        blobs[:, 2] = blobs[:, 2] * np.sqrt(2)

        # Create and add a point layer
        viewer.add_layer(
            Points(data=blobs[:, 0:2],
                   ndim=2,
                   name="Blobs",
                   size=blobs[:, 2],
                   edge_width=5,
                   edge_color="white",
                   face_color=np.random.rand(3))
                   )

        # Display histogram of blob sizes
        fig, ax = plt.subplots()
        ax.hist(
            blobs[:, 2],
            bins=np.linspace(min_sigma, max_sigma, num_sigma),
        )
        ax.set_xlabel("Blob radius (sigma * sqrt(2))")
        ax.set_ylabel("Blob count")
        fig.show()

    return plugin

@napari_hook_implementation
def napari_experimental_provide_dock_widget():
    # you can return either a single widget, or a sequence of widgets
    return [object_detector_and_analyzer_wrapper]
```

Since from the [documentation](https://napari.org/plugins/stable/hook_specifications.html#napari.plugins.hook_specifications.napari_experimental_provide_dock_widget) we know that `napari_experimental_provide_dock_widget()` "*provides functions that return widgets to be docked in the viewer*", we know that the concept is similar to `napari_get_reader()`, which doesn't read a file but provides a function that does it.   

This time we will use a slightly different way of implementing a similar functionality we had in the reader example: we will create a `object_detector_and_analyzer_wrapper` function that internally uses the `@magicgui` decorator on the real plug-in `plugin()`[^magicgui_configuration] and returns it (see line: `return plugin` at the end of the `object_detector_and_analyzer_wrapper` function).

[^magicgui_configuration]: Please notice that we specify a custom `step=0.01` for the `threshold` argument). For a more detailed explanation of the `@magicgui` operator and its parameters, please see https://napari.org/magicgui/usage/configuration.html.

In the `plugin()` function we implement the actual analysis. We use the **Laplacian of Gaussian (LoG) blob detector** implementation by **scikit-image** (see [https://scikit-image.org/docs/dev/api/skimage.feature.html#skimage.feature.blob_log](https://scikit-image.org/docs/dev/api/skimage.feature.html#skimage.feature.blob_log) and [https://en.wikipedia.org/wiki/Blob_detection#The_Laplacian_of_Gaussian](https://en.wikipedia.org/wiki/Blob_detection#The_Laplacian_of_Gaussian)) to find as many bright objects of different sizes in our image.

The `blob_log()` function takes an image, `min_sigma`, `max_sigma`, `num_sigma` and `threshold` as input parameters, and we expose those through our widget (please notice that we use the @magicgui decorator to change the `step` of the threshold widget) . 

The `img_layer` argument that napari passes to our plugin is of type `napari.layers.Image`. It is an **image layer**, and it contains the actual image data along with other information. Since the `blob_log()` function does not know how to process a layer, we need to extract the image data from `img_layer` using its `.data` property.

```python
image = np.asarray(img_layer.data)
```

We make sure to convert the image to a Numpy array to be compatible with `blob_log()`.

Let's test our plug-in by opening the **Hubble Deep Field (RGB)** sample image from `File > Open Sample > scikit-image > Hubble Deep Field (RGB)` and running our plug-in from `Plugins > napari-object-analyzer > object_detector_and_analyzer_wrapper`. The plug-in opens in the right-hand side of the napari viewer.

![Our analysis widget with the parameters for `blob_log().`](images/napari_analysis_widget.png){height=100px}

`blob_log` will test all sigmas in out defined range for the strongest responses and return the locations and *winning* sigma for all the detected `blobs`. `blobs` is an $Nx3$ matrix with `x`, `y` and `sigma` as columns. The radius of a blob is calculated from its sigma with $r = \sigma \cdot \sqrt{2}$. To visualize the results, we create a **Points layer** with the first two columns of `blobs` as `data` (i.e. the positions) and the updated `r` column as `size`. We also assign a random color to make subsequent runs easily distinguishable. 

This is the result of our default run.

![Graphical result of the blob detection](images/napari_analysis.png){height=300px}

We also display a histogram of the blob radii.

![Histogram of blob sizes.](images/napari_analysis_hist.png){height=200px}

Please feel free to play with the parameters and see what changes in the output and the histogram!

### Committing our changes

When we are done with our implementaion, we commit our changes to git:

```bash
$ git status
On branch main
Your branch is up to date with 'origin/main'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   noa/_dock_widget.py
        modified:   noa/_function.py
        modified:   noa/_reader.py
        modified:   noa/_writer.py

no changes added to commit (use "git add" and/or "git commit -a")

$ git add .
$ git commit -m "Implement plug-ins."
```

If you have set up your remote on [https://github.com](https://github.com), you can push your changes.

```bash
$ git push -u origin main
```

We will leave publishing our plug-in to the [napari hub](https://www.napari-hub.org) for another course!

