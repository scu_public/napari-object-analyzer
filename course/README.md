# Requirements

To build the documentation you will need:

- Ubuntu:

    - texlive: `sudo apt install texlive`
    - texlive-xetex: `sudo apt install texlive-xetex`
    - lmodern: `sudo apt install lmodern`
    - pandoc: `sudo apt install pandoc` or  `conda install pandoc`
    - pandoc-citeproc: `sudo apt install pandoc` or  `conda install pandoc-citeproc`

* macOS:

    - MacTex: `brew cask install mactex`
    - pandoc: `brew install pandoc` or `conda install pandoc`
    - pandoc-citeproc: `brew install pandoc-citeproc`  or  `conda install pandoc-citeproc`

# Build

Usage: 
`build.sh ${document_name}`
`build.sh ${document_name} YYYY-MM-DD`

`${document_name}` is the name of the document to build without extension. `build.sh` will parse `${document_name}.txt` for Markdown files to be compiled into the final `${document_name}.pdf`.

Currently, `writing_napari_plugins` is the only document.

```bash
$ ./build.sh writing_napari_plugins 2021-09-26
```

The resulting `writing_napari_plugins.pdf` file will be in `build/`.
