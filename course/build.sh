# USAGE: 
# [1]   ./build.sh Document         # e.g. ./build.sh writing_napari_plugins
# [2]   ./build.sh Document date    # e.g. ./build.sh writing_napari_plugins 2021-09-05
#
# If the date argument is not specified, today's date in the form YYYY-MM-DD will be used. 

# Collect arguments
DOCUMENT=$1
INPUT_FILE_LIST_TXT=${DOCUMENT}.txt
CURRENT_AUX_FILE=${DOCUMENT}.aux
PREVIOUS_AUX_FILE=${DOCUMENT}_prev.aux
OUTPUT_TEX_FILE=${DOCUMENT}.tex
OUTPUT_PDF_FILE=${DOCUMENT}.pdf

# Inform
echo "Processing ${INPUT_FILE_LIST_TXT} to produce ${OUTPUT_TEX_FILE} and ${OUTPUT_PDF_FILE}."

# Markdown to Latex
echo "Converting Markdown to Latex..."
pandoc                                 \
  --from         markdown              \
  --to           latex                 \
  --standalone                         \
  --defaults     defaults.yaml         \
  --template     template.tex          \
  --out          ${OUTPUT_TEX_FILE}    \
  --pdf-engine   xelatex               \
  --toc                                \
  `cat ${INPUT_FILE_LIST_TXT}`

# Making figures non-floating
echo "Making figures non-floating..."
machine=`uname -s`
sed_str='s/\\begin{figure}/\\begin{figure}[H]/g'
case $machine in
Linux*)
  sed -i -e ${sed_str} ${OUTPUT_TEX_FILE}
  ;;
Darwin*)
  sed -i '' -e ${sed_str} ${OUTPUT_TEX_FILE}
  ;; 
CYGWIN*)
  sed -i -e ${sed_str} ${OUTPUT_TEX_FILE};
  ;;
MINGW*)
  sed -i -e ${sed_str} ${OUTPUT_TEX_FILE}
  ;;
*)
  echo ERROR: Unsupported platform; 
  exit 1
  ;;
esac

# Add the date
echo "Set the date..."
if [ $# -eq 2 ]
then
  sed_str="s/\\date{}/\\date{$2}/g"
else
  sed_str="s/\\date{}/\\date{`date +%Y-%m-%d`}/g"
fi
case $machine in
Linux*)
  sed -i -e ${sed_str} ${OUTPUT_TEX_FILE}
  ;;
Darwin*)
  sed -i '' -e ${sed_str} ${OUTPUT_TEX_FILE}
  ;; 
CYGWIN*)
  sed -i -e ${sed_str} ${OUTPUT_TEX_FILE};
  ;;
MINGW*)
  sed -i -e ${sed_str} ${OUTPUT_TEX_FILE}
  ;;
*)
  echo ERROR: Unsupported platform; 
  exit 1
  ;;
esac

# Converting to PDF
# We run the conversion several times until no changes
# can be found in the AUX file.
echo "Converting to PDF..."
RED='\033[0;31m'
NC='\033[0m' # No Color
touch ${PREVIOUS_AUX_FILE}
RUNMORE=1
while [ $RUNMORE -eq 1 ]; do
  printf "${RED}Performing conversion.\n${NC}"
	xelatex -interaction=nonstopmode ${OUTPUT_TEX_FILE}
	cmp --silent ${CURRENT_AUX_FILE} ${PREVIOUS_AUX_FILE}  && RUNMORE=0 || RUNMORE=1
  cp ${CURRENT_AUX_FILE} ${PREVIOUS_AUX_FILE};
done
echo "Conversion completed."

# Move to 'build' directory
if [ ! -d "build" ]; then
  mkdir build
fi
mv ./${OUTPUT_PDF_FILE} ./build/${OUTPUT_PDF_FILE}

# Clean up
rm -f ${DOCUMENT}.aux ${DOCUMENT}_prev.aux ${DOCUMENT}.log ${DOCUMENT}.out ${DOCUMENT}.toc ${DOCUMENT}.tex

echo "All done."
