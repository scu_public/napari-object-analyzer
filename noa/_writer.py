"""
This module is an example of a barebones writer plugin for napari

It implements the ``napari_get_writer`` and ``napari_write_image`` hook specifications.
see: https://napari.org/docs/dev/plugins/hook_specifications.html

Replace code below according to your needs
"""
import numpy as np
from napari_plugin_engine import napari_hook_implementation


def writer_function(path, layer_data):
    """The writer function would be returned by napari_get_writer()"""
    return None


@napari_hook_implementation
def napari_get_writer(path, layer_types):
    """If 'Save all layers...' is picked, we ignore it."""
    return None


@napari_hook_implementation
def napari_write_image(path, data, meta):
    """This is called when 'Save selected layer(s)...' is picked."""

    # If we can save successfully, we return path;
    # otherwise, we return None
    if str(path).endswith(".npy"):
        try:
            # For simplicity, we ignore the metadata
            np.save(path, data)
        except:
            return None
        return path
    return None
