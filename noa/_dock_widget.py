"""
This module is an example of a barebones QWidget plugin for napari

It implements the ``napari_experimental_provide_dock_widget`` hook specification.
see: https://napari.org/docs/dev/plugins/hook_specifications.html

Replace code below according to your needs.
"""
import napari
from napari.layers import Points
from napari_plugin_engine import napari_hook_implementation
from qtpy.QtWidgets import QWidget, QHBoxLayout, QPushButton
from magicgui import magic_factory
from skimage.color import rgb2gray
from skimage.feature import blob_log
import numpy as np
from magicgui import magicgui
import matplotlib.pyplot as plt


class ExampleQWidget(QWidget):
    # your QWidget.__init__ can optionally request the napari viewer instance
    # in one of two ways:
    # 1. use a parameter called `napari_viewer`, as done here
    # 2. use a type annotation of 'napari.viewer.Viewer' for any parameter
    def __init__(self, napari_viewer):
        super().__init__()
        self.viewer = napari_viewer

        btn = QPushButton("Click me!")
        btn.clicked.connect(self._on_click)

        self.setLayout(QHBoxLayout())
        self.layout().addWidget(btn)

    def _on_click(self):
        print("napari has", len(self.viewer.layers), "layers")


@magic_factory
def example_magic_widget(img_layer: "napari.layers.Image"):
    print(f"you have selected {img_layer}")


def object_detector_and_analyzer_wrapper():
    """Wrapper to the plug-in: napari will call this function
    to get the dock widget."""

    @magicgui(
        img_layer=dict(label='Image'),
        min_sigma=dict(label="Min sigma"),
        max_sigma=dict(label="Max sigma"),
        num_sigma=dict(label="Num sigma"),
        threshold=dict(label="Threshold", step=0.01),
    )
    def plugin(
            img_layer: napari.layers.Image,
            min_sigma: int = 3,
            max_sigma: int = 15,
            num_sigma: int = 13,
            threshold: float = 0.05
    ):
        """Simple detection and analysis function."""

        # If there is no image loaded, return here
        if img_layer is None:
            return

        # Get the viewer
        viewer = napari.current_viewer()

        # Make sure we work on a gray-value image
        image = np.asarray(img_layer.data)
        if image.ndim > 2 and image.shape[2] == 3:
            image = rgb2gray(image)

        # Run blob detector based on Laplacian of Gaussian
        blobs = blob_log(image,
                         min_sigma=min_sigma,
                         max_sigma=max_sigma,
                         num_sigma=num_sigma,
                         threshold=threshold)

        # We compute the radii from the 3rd columns (that contains
        # the winning sigma)
        blobs[:, 2] = blobs[:, 2] * np.sqrt(2)

        # Create and add a point layer
        viewer.add_layer(
            Points(data=blobs[:, 0:2],
                   ndim=2,
                   name="Blobs",
                   size=blobs[:, 2],
                   edge_width=5,
                   edge_color="white",
                   face_color=np.random.rand(3))
                   )

        # Display histogram of blob sizes
        fig, ax = plt.subplots()
        ax.hist(
            blobs[:, 2],
            bins=np.linspace(min_sigma, max_sigma, num_sigma),
        )
        ax.set_xlabel("Blob radius (sigma * sqrt(2))")
        ax.set_ylabel("Blob count")
        fig.show()

    return plugin


@napari_hook_implementation
def napari_experimental_provide_dock_widget():
    # you can return either a single widget, or a sequence of widgets
    return [ExampleQWidget, example_magic_widget, object_detector_and_analyzer_wrapper]
