from skimage import data
from skimage.color import rgb2gray
from napari_plugin_engine import napari_hook_implementation


def _prepare_data():
    image = data.hubble_deep_field()
    image_gray = rgb2gray(image)
    return [(image_gray, {'name': 'Hubble Deep Field (gray)'})]

@napari_hook_implementation
def napari_provide_sample_data():
    return {
        'Napari plug-in demo': _prepare_data
    }
